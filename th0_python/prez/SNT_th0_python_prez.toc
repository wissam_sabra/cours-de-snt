\babel@toc {english}{}\relax 
\beamer@sectionintoc {1}{Variables et affectation}{17}{0}{1}
\beamer@sectionintoc {2}{Les fonctions}{27}{0}{2}
\beamer@sectionintoc {3}{Structures conditionnelles}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{if}{35}{0}{3}
\beamer@subsectionintoc {3}{2}{if \dots else}{45}{0}{3}
\beamer@subsectionintoc {3}{3}{if \dots elif \dots else}{51}{0}{3}
\beamer@sectionintoc {4}{Les boucles bornées}{62}{0}{4}
\beamer@sectionintoc {5}{Les boucles non bornées}{72}{0}{5}
