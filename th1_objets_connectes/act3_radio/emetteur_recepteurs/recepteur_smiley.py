from microbit import *
import radio
radio.config(group=...)
...

...:
    message = radio.receive()
    if message == "HAPPY":
        display.show(Image.HAPPY)
    if message == "SAD":
        display.show(Image.SAD)
    sleep(1000)
    display.clear()
