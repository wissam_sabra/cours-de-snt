 ############################################
#      Emetteur / recepteur de texte       #
############################################
# Initialisation
from microbit import *
import radio
radio.on()
radio.config(group = 10, length = 251)
#
#--------------------------------------------------
# Boucle Infinie -> True est toujours vrai ;)
while True:
    message_recu = radio.receive()
    if message_recu != None:
        print("RECU : ", message_recu)
    elif button_a.is_pressed():
        message_envoye = input("ENVOIE : ")
        radio.send(message_envoye)
    sleep(50)