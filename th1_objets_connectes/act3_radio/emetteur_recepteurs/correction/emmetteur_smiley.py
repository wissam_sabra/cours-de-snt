############################################
#             Emetteur de smiley           #
############################################
#-------------------------------------------
# Initialisation
from microbit import *
import radio
radio.config(group=1)
radio.on()
#
#---------------------------------------------
# Boucle Infinie -> True est toujours vrai ;)
while True:
    if button_a.is_pressed():
        radio.send("HAPPY")
    elif button_b.is_pressed():
        radio.send("SAD")
    sleep(10)
