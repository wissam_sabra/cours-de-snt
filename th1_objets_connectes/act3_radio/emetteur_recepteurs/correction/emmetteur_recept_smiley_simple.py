############################################
#      Emetteur / recepteur de smiley      #
############################################

######## Initialisation ###############
from microbit import *
import radio
radio.config(group=12)
radio.on()

####### Boucle infinie ################
while True:
    message = radio.receive()
    if message == "OUI":
        display.show(Image.HAPPY)
    elif message == "NON":
        display.show(Image.SAD)
    else:
        if button_a.is_pressed():
            radio.send("OUI")
        elif button_b.is_pressed():
            radio.send("NON")
    sleep(10)
    display.clear()


