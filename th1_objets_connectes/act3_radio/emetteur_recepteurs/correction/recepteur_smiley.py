############################################
#            recepteur de smiley           #
############################################
#-------------------------------------------
# Initialisation
from microbit import *
import radio
radio.config(group=1)
radio.on()
#---------------------------------------------
# Boucle Infinie -> True est toujours vrai ;)
while True:
    message = radio.receive()
    if message == "HAPPY":
        display.show(Image.HAPPY)
    elif message == "SAD":
        display.show(Image.SAD)
    else :
        display.clear()
    sleep(10)
    display.clear()
