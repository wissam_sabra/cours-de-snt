############################################
#      Emetteur / recepteur de smiley      #
############################################
#
# Initialisation
from microbit import *
import radio
radio.config(group=5)
radio.on()
#
#------------------------------
# Boucle Infinie -> Parce que True est toujours vrai ;)
while True:
    message = radio.receive()
    if message == None:
        if button_a.is_pressed():
            radio.send("HAPPY")
        elif button_b.is_pressed():
            radio.send("SAD")
    elif message == "HAPPY":
        display.show(Image.HAPPY)
    elif message == "SAD":
        display.show(Image.SAD)
    sleep(10)
    display.clear()