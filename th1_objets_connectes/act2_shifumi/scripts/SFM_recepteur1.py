####################################
# Pour tricher: appuyer sur A + B #
####################################
from microbit import *
import radio
radio.config(group=27)
radio.on()

feuille = Image("99999:90009:90009:90009:99999")
ciseaux = Image("99009:99090:00900:99090:99009")
pierre = Image("00000:09990:09990:09990:00000")
#message = radio.receive()
while True:
    display.show("3")
    sleep(1000)
    display.show("2")
    sleep(1000)
    display.show("1")
    sleep(1000)
    message = radio.receive()
    if message != None:
        choixAdverse = message
        if button_a.is_pressed():
            # si on appuie aussi sur B
            if button_b.is_pressed():
                if choixAdverse == "pierre":
                    display.show(feuille)
                    sleep(3000)
                elif choixAdverse == "feuille":
                    display.show(ciseaux)
                    sleep(3000)
                elif choixAdverse == "ciseaux":
                    display.show(pierre)
                    sleep(3000)
            #si on appuie que sur A
            else:
                display.show(pierre)
                sleep(3000)
        # Si on appuie que sur B
        elif button_b.is_pressed():
            display.show(feuille)
            sleep(3000)
        # Si on appuie sur aucun bouton
        else:
            display.show(ciseaux)
            sleep(3000)
    #Nécessaire si message non recu pour rester synchronisé --> 1er tour ne fonctionne pas
    else:
        display.show(Image.HAPPY)
        sleep(3000)
    sleep(10)