######## Intitialisation ###########
from microbit import *

feuille = Image("99999:90009:90009:90009:99999")
ciseaux = Image("99009:99090:00900:99090:99009")
pierre = Image("00000:09990:09990:09990:00000")


######### Boucle infinie ############
while True:
    # Afficher le décompte
    display.show("3")
    sleep(1000)
    display.show("2")
    sleep(1000)
    display.show("1")
    sleep(1000)
    if button_a.is_pressed():
        display.show(pierre)  
    elif button_b.is_pressed():
        display.show(feuille) 
    else: # Si on appuie sur aucun des deux
        display.show(ciseaux):
    sleep(3000)
