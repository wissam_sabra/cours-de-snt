from microbit import *
import radio
radio.config(group=27)
radio.on()

feuille = Image("99999:90009:90009:90009:99999")
ciseaux = Image("99009:99090:00900:99090:99009")
pierre = Image("00000:09990:09990:09990:00000")

while True:
    choixAdverse = radio.receive()
    if choixAdverse != None:
        if choixAdverse == "pierre":
            display.show(feuille)
            sleep(3000)
        elif choixAdverse == "feuille":
            display.show(ciseaux)
            sleep(3000)
        elif choixAdverse == "ciseaux":
            display.show(pierre)
            sleep(3000)
    else:
        display.show(Image.HAPPY)
    sleep(10)
