\babel@toc {french}{}\relax 
\contentsline {section}{\numberline {1}commander une microbit}{2}{section.1}%
\contentsline {section}{\numberline {2}la bibliothèque microbit}{2}{section.2}%
\contentsline {section}{\numberline {3}pins et boutons}{2}{section.3}%
\contentsline {subsection}{\numberline {3.1}Faire une pause}{2}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Entrée}{2}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Sortie}{2}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Les boutons}{3}{subsection.3.4}%
\contentsline {section}{\numberline {4}l'écran}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Désactiver l’écran}{3}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Effacer l'écran}{3}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Afficher des images prédéfinies}{3}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}délouler du texte}{3}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Afficher des caractères}{3}{subsection.4.5}%
\contentsline {section}{\numberline {5}interagir avec la microbit}{4}{section.5}%
\contentsline {section}{\numberline {6}communication par radio}{4}{section.6}%
\contentsline {subsection}{\numberline {6.1}Importation du module}{4}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Choix du canal de communication}{4}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Message texte}{4}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}Recevoir un message}{4}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}Envoyer un message}{5}{subsection.6.5}%
