(TeX-add-style-hook
 "SNT_OC_Act1"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exam" "french" "english")))
   (TeX-run-style-hooks
    "latex2e"
    "mes_packages"
    "style_act"
    "exam"
    "exam10"))
 :latex)

