##################
# Initialisation #
##################
#
# importation de la bibliothèque de fonction de la microbit
from microbit import *
#
###################
# Boucle infinie  #
###################
#
while True:
    if button_a.is_pressed():
        display.show(Image.HAPPY)
    elif button_b.is_pressed():
        display.show(Image.SAD)
    else:
        display.clear()
