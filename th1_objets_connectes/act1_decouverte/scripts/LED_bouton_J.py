##################
# Initialisation #
##################
#
# importation de la bibliothèque de fonction de la microbit
from microbit import *
# Désactiver la matrice de LED pour rendre le pin3 disponible
display.off()
# Choix des pins
pinV = pin3
pinJ = pin2
pinR = pin1
pinBouton = pin4
# Extinction de toutes les LED
pinR.write_digital(0)
pinV.write_digital(0)
pinJ.write_digital(0)
#
##################
# Boucle infinie #
##################
#
while True:
    if pinBouton.read_digital()==1:
        pinJ.write_digital(1)
    else:
        pinJ.write_digital(0)
    sleep(10)








