* [[./th0_python][Thème 0 : python]]
- notebook jupyter
- présentation Beamer

*Remarques:*
- Le notebook peut être traité en une seul fois comme un thème indépendant, ou
bien on peut revenir au différentes parties à chaques fois qu’il est nécessaire
d’introduire une notion dans l’année. Cette possibilité correspond plus à l’esprit du
programmme.
- On peut envisager de découper le notebook en plusieurs petits correspondant aux
  différentes parties.

* [[./th1_objets_connectes][Thème 1 : les objets connectés]]
Les microbits sont utilisées comme support pour ce thème.
- [[file:th1_objets_connectes/act1_decouverte/][act 1 : découverte de la microbit]]
- [[file:th1_objets_connectes/act2_shifumi/][act 2 : shi-fu-mi]]
- [[file:th1_objets_connectes/act3_radio/][act 3 : comminication par radio]]

* Thème 2 : Internet

* Thème 3 : le web

* Thème 4 : données

* Thème 5 : cartographie

* Thème 6 : photos numériques
